<?php session_start(); ?>
<html>
    <head><title>Info</title></head>
    <body>
        <?php
            function existe($numero,$contra){
                foreach($_SESSION['Alumno'] as $sesiones) {
                    if ($sesiones['num_cta'] == $numero && $sesiones['contrasena'] == $contra){ return TRUE; }
                }
                return FALSE;
            }

            if (count($_POST) == 2){
                if (!existe($_POST['num_cta'],$_POST['contrasena'])){
                    header("Location: http://127.0.0.1/ejemplos/tarea/login.php");
                } else {
                    for($sesiones = 0; $sesiones < count($_SESSION['Alumno']); $sesiones++) {
                        $_SESSION['Alumno'][$sesiones]['activo'] = 0;
                        if ($_SESSION['Alumno'][$sesiones]['num_cta'] == $_POST['num_cta']){ 
                            $_SESSION['Alumno'][$sesiones]['activo'] = 1;
                        }
                    }
                }
            }
            else {
                array_push($_SESSION['Alumno'],[
                    'num_cta' => $_POST['num_cta'],
                    'nombre' => $_POST['nombre'],
                    'primer_apellido' => $_POST['primer_apellido'],
                    'segundo_apellido' => $_POST['segundo_apellido'],
                    'contrasena' => $_POST['contrasena'],
                    'genero' => $_POST['sexo'],
                    'fecha_nac' => $_POST['date'],
                    'activo' => 0
                ]); 
            }
            foreach($_SESSION['Alumno'] as $sesiones) {
                if ($sesiones['activo'] == 1){
                    $num_activo = $sesiones['num_cta'];
                    $nombre_activo = $sesiones['nombre'];
                    $fecha_activo = $sesiones['fecha_nac'];
                }
            }
        ?>
        <nav>
            <ul>
                <li><a href="./login.php">Home</a></li>
                <li><a href="./formulario.php">Registrar Alumno</a></li>
                <li><a href="./login.php">Cerrar Sesión</a></li>
            </ul>
        </nav>
        <h1>Usuario Autenticado</h1>
        <section>
            <article><?php echo "$nombre_activo"; ?></article>
            <article>Información</article>
            <article>Numero de cuenta: <?php echo "$num_activo"; ?></article>
            <article>Fecha de Nacimiento: <?php echo "$fecha_activo"; ?></article>
        </section>
        <h1>Datos Guardados</h1>
        <section>
            <article># Nombre Fecha de Nacimiento</article>
            <?php
                foreach($_SESSION['Alumno'] as $sesiones) {
                    $cuenta = $sesiones['num_cta'];
                    $nombre = $sesiones['nombre'];
                    $fecha = $sesiones['fecha_nac'];

                    echo "<article>$cuenta $nombre $fecha\n</article>";
                }
            ?>
        </section>
    </body>
</html>