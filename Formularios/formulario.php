<?php session_start(); ?>
<html>
    <head>
        <title>Formulario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

    <body>
        <div>
            <form action="info.php?" method="POST">
                <label class="form-label" for="num_cta">Número de Cuenta:</label>
                <input class="form-input" type="text" name="num_cta" placeholder="Número de cuenta">
                <br>
                <label class="form-label" for="nombre">Nombre</label>
                <input class="form-input" type="text" name="nombre" placeholder="Nombre">
                <br>
                <label class="form-label" for="primer_apellido">Primer Apellido</label>
                <input class="form-input" type="text" name="primer_apellido" placeholder="Primer Apellido">
                <br>
                <label class="form-label" for="segundo_apellido">Segundo Apellido</label>
                <input class="form-input" type="text" name="segundo_apellido" placeholder="Segundo Apellido">
                <br>
                <label class="form-label">Sexo</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="H" checked>
					<i class="form-icon"></i> Hombre
				</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="M">
					<i class="form-icon"></i> Mujer
				</label>
                <label class="form-radio">
					<input type="radio" name="sexo" value="O">
					<i class="form-icon"></i> Otro
				</label>
                <br>
                <label class="form-label" for="input-date">Fecha de Nacimiento</label>
				<input name="date" class="form-input " type="date" id="input-date" placeholder="Fecha de Nacimiento">
                <br>
                <label class="form-label" for="contrasena">Contraseña</label>
                <input class="form-input"type="password" name="contrasena" placeholder="Contraseña">
                <br>
                <input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>
            </form>
        </div>
    </body>
</html>